package com.wisniewski.dawid.monumentsfinderandroidapp.models;

/**
 * Created by user on 2017-05-11.
 */

public class MonumentModel {
    public String id;
    public String name;
    public String description;
    public String imgSrc;
    public String coordinates;
    public String source;
    public double latitude;
    public double longitude;
}
