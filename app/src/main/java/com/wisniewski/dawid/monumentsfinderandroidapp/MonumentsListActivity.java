package com.wisniewski.dawid.monumentsfinderandroidapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wisniewski.dawid.monumentsfinderandroidapp.models.MonumentModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MonumentsListActivity extends FragmentActivity implements OnTaskCompleted, OnMapReadyCallback {
    ProgressDialog progress;
    LinearLayout layout;
    static final String API_URL = "http://10.0.2.2/app/api/Monuments";
    private ArrayList<MonumentModel> monuments;
    Context context;
    ListView monumentsList;
    private GoogleMap map;
    private AtomicBoolean marked;
    private String address;
    private boolean useUserLocation;
    private double longitude;
    private double latitude;
    private int radius;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        marked = new AtomicBoolean(false);
        context = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monuments_list);
        progress = ProgressDialog.show(MonumentsListActivity.this, getString(R.string.please_wait),
                getString(R.string.downloading), true);
        Intent intent = getIntent();
        address = intent.getStringExtra("address");
        useUserLocation = intent.getBooleanExtra("useUserLocation", false);
        radius = intent.getIntExtra("radius", 10);
        if (useUserLocation)
            initCordsWithLocation();
        else
            initCordsWithAddress();

        new RetrieveFeedTask(this).execute();
        layout = (LinearLayout) findViewById(R.id.mainLayout);
        monumentsList = (ListView) findViewById(R.id.monuments);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initCordsWithLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location currentLocation = getLastBestLocation();
        if(currentLocation!=null) {
            latitude = currentLocation.getLatitude();
            longitude = currentLocation.getLongitude();
        } else {
            latitude = 0;
            longitude = 0;
        }
    }

    private void initCordsWithAddress() {
        // source: https://stackoverflow.com/questions/9698328/how-to-get-coordinates-of-an-address-in-android/9698408#9698408
        Geocoder geocoder = new Geocoder(this);
        try {
            List<Address> addresses = geocoder.getFromLocationName(address, 1);
            if (addresses.size() > 0) {
                latitude = addresses.get(0).getLatitude();
                longitude = addresses.get(0).getLongitude();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onTaskCompleted() {
        monumentsList.setAdapter(new MonumentsAdapter(this, this.monuments));
        monumentsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MonumentModel monumentModel = monuments.get(position);
                Intent intent = new Intent(MonumentsListActivity.this, MonumentActivity.class);
                intent.putExtra("id", monumentModel.id);
                intent.putExtra("coordinates", monumentModel.coordinates);
                intent.putExtra("description", monumentModel.description);
                intent.putExtra("imgSrc", monumentModel.imgSrc);
                intent.putExtra("source", monumentModel.source);
                intent.putExtra("monumentLong", monumentModel.longitude);
                intent.putExtra("monumentLat", monumentModel.latitude);
                intent.putExtra("userLong", longitude);
                intent.putExtra("userLat", latitude);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (!marked.get() && monuments != null)
            addMarkers();
        LatLng point = new LatLng(latitude, longitude);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 10));

        map.addMarker(new MarkerOptions().position(point).title("Your position").icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
    }

    private void addMarkers() {
        marked.set(true);
        for (MonumentModel m :
                monuments) {
            LatLng monumentPosition = new LatLng(m.latitude, m.longitude);
            map.addMarker(new MarkerOptions().position(monumentPosition).title(m.name));
        }
    }

    private Location getLastBestLocation() {
        // source: https://stackoverflow.com/questions/1513485/how-do-i-get-the-current-gps-location-programmatically-in-android/12963889#12963889
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (null != locationGPS) { GPSLocationTime = locationGPS.getTime(); }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }

        if ( 0 < GPSLocationTime - NetLocationTime ) {
            return locationGPS;
        }
        else {
            return locationNet;
        }
    }

    // source: http://www.androidauthority.com/use-remote-web-api-within-android-app-617869/
    class RetrieveFeedTask extends AsyncTask<Void, Void, String>
    {
        private OnTaskCompleted listener;

        public RetrieveFeedTask(OnTaskCompleted listener)
        {
            this.listener = listener;
        }

        protected void onPreExecute()
        {
        }

        protected String doInBackground(Void... urls)
        {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try
            {
                String strUrl = API_URL+"/near?radius="+radius+"&latitude="+latitude+"&longitude="+longitude;
                URL url = new URL(strUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try
                {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;

                    while ((line = bufferedReader.readLine()) != null)
                    {
                        stringBuilder.append(line).append("\n");
                    }

                    bufferedReader.close();
                    return stringBuilder.toString();
                }
                finally
                {
                    urlConnection.disconnect();
                }
            }
            catch(Exception e)
            {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response)
        {
            if(response == null)
            {
                response = "THERE WAS AN ERROR";
                Log.i("INFO", response);
                return;
            }

            progress.dismiss();
            //monuments = new ArrayList<>();
            //https://stackoverflow.com/questions/5554217/google-gson-deserialize-listclass-object-generic-type
            Type listType = new TypeToken<ArrayList<MonumentModel>>(){}.getType();
            monuments = new Gson().fromJson(response, listType);
            if(!marked.get() && map!=null)
                addMarkers();
            //responseView.setText(response);
            listener.onTaskCompleted();
            //layout.addView(tl,0);
        }
    }
}
