package com.wisniewski.dawid.monumentsfinderandroidapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText address;
    private EditText radius;
    private boolean useUserLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        address = (EditText) findViewById(R.id.address);
        radius = (EditText) findViewById(R.id.radius);
        radius.setText("10");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    public void useLocationClicked(View view) {
        useUserLocation = ((CheckBox) view).isChecked();
        address.setFocusable(!useUserLocation);
        address.setCursorVisible(!useUserLocation);
        if(useUserLocation) {
            address.setText("");
            address.setHint(R.string.disabled);
        }//if
        else {
            address.setHint(R.string.type_address);
            address.setFocusableInTouchMode(true);
        }//else
    }//useLocationClicked()

    public void findClicked(View view) {
        Intent intent = new Intent(this, MonumentsListActivity.class);
        intent.putExtra("address", address.getText().toString());
        intent.putExtra("useUserLocation", useUserLocation);
        intent.putExtra("radius", Integer.parseInt(radius.getText().toString()));
        startActivity(intent);
    }//findClicked()
}
