package com.wisniewski.dawid.monumentsfinderandroidapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wisniewski.dawid.monumentsfinderandroidapp.models.MonumentModel;

public class MonumentActivity extends AppCompatActivity
{
    private ImageView monument_image;
    private MonumentModel monument;
    private double longitude, latitude;
    private TextView name, coordinates, description;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monument);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        monument = new MonumentModel();
        monument.id = intent.getStringExtra("id");
        monument.name = intent.getStringExtra("name");
        monument.coordinates = intent.getStringExtra("coordinates");
        monument.description = intent.getStringExtra("description");
        monument.source = intent.getStringExtra("source");
        monument.imgSrc = intent.getStringExtra("imgSrc");
        monument.latitude = intent.getDoubleExtra("monumentLat", 0);
        monument.longitude = intent.getDoubleExtra("monumentLong", 0);
        latitude = intent.getDoubleExtra("userLat", 0);
        longitude = intent.getDoubleExtra("userLong", 0);

        monument_image = (ImageView) findViewById(R.id.monument_draw);
        name = (TextView) findViewById(R.id.name);
        coordinates = (TextView) findViewById(R.id.coordinates);
        description = (TextView) findViewById(R.id.description);
        name.setText(monument.name);
        coordinates.setText(getString(R.string.coordinates)+": "+monument.coordinates);
        description.setText(monument.description);

        new ImageDownloader(monument_image).execute(monument.imgSrc);
    }

    public void onMoreClick(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(monument.source));
        startActivity(intent);
    }

    public void onShowOnMapClick(View view) {
        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
        intent.putExtra("monumentLong", monument.longitude);
        intent.putExtra("monumentLat", monument.latitude);
        intent.putExtra("userLong", longitude);
        intent.putExtra("userLat", latitude);
        intent.putExtra("name", monument.name);
        startActivity(intent);
    }

    public void onSetPathClick(View view) {

    }

}
