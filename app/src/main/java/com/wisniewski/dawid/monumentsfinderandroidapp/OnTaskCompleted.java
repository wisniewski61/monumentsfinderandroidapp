package com.wisniewski.dawid.monumentsfinderandroidapp;

/**
 * Created by user on 2017-05-11.
 */

public interface OnTaskCompleted {
    void onTaskCompleted();
}
