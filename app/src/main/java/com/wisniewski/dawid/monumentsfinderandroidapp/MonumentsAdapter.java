package com.wisniewski.dawid.monumentsfinderandroidapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wisniewski.dawid.monumentsfinderandroidapp.models.MonumentModel;

import java.util.ArrayList;

/**
 * Created by user on 2017-05-11.
 */

//source: https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView
public class MonumentsAdapter extends ArrayAdapter<MonumentModel>{
    public MonumentsAdapter(Context context, ArrayList<MonumentModel> monuments) {
        super(context, 0, monuments);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        MonumentModel monument = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.monuments_layout, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
        // Populate the data into the template view using the data object
        tvName.setText(monument.name);
        tvHome.setText(monument.description.substring(0, 50)+"...");
        // Return the completed view to render on screen
        return convertView;
    }
}
