package com.wisniewski.dawid.monumentsfinderandroidapp;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;
import java.net.URL;

public class ImageDownloader extends AsyncTask<String, Void, Drawable>
{
    private ImageView imageToLoad;

    public ImageDownloader(ImageView image)
    {
        imageToLoad = image;
    }

    @Override
    protected Drawable doInBackground(String... params)
    {
        Drawable draw = null;
        try
        {
            URL thumb_u = new URL(params[0]);
            draw = Drawable.createFromStream(thumb_u.openStream(), "src");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return draw;
    }

    @Override
    protected void onPostExecute(Drawable draw)
    {
        imageToLoad.setImageDrawable(draw);
    }
}